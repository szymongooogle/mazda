/*
 * touch.h
 *
 *  Created on: 2018. 12. 7.
 *      Author: chand
 */

#ifndef INC_TOUCH_H_
#define INC_TOUCH_H_

extern void initTouch();
extern void input_sync();
extern uint8_t *getTouchPtr();
extern uint8_t *getTouchQualityKeyPtr();

void tpd_down(uint16_t x, uint16_t y, uint16_t p);
void tpd_up(uint16_t p);

#endif /* INC_TOUCH_H_ */
