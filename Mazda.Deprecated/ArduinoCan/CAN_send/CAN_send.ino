 // CAN Send Example
//

#include <mcp_can.h>
#include <SPI.h>

MCP_CAN CAN0(15); //Wemos mini D8
byte cnt = 0;
bool ext = false;
void setup()
{
  Serial.begin(115200);

  // Initialize MCP2515 running at 16MHz with a baudrate of 500kb/s and the masks and filters disabled.
  if(CAN0.begin(MCP_ANY, CAN_125KBPS, MCP_8MHZ) == CAN_OK) Serial.println("MCP2515 Initialized Successfully!");
  else Serial.println("Error Initializing MCP2515...");

  CAN0.setMode(MCP_NORMAL);   // Change to normal mode to allow messages to be transmitted
}

byte data[8] = {0x00, 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};

void loop()
{
  // send data:  ID = 0x100, Standard CAN Frame, Data length = 8 bytes, 'data' = array of data bytes to send

cnt++;

if(cnt> 25)
{
  cnt = 0;
  ext = !ext;
}

data[0] = cnt;
data[4] = (byte) random(250);

  byte sndStat = CAN0.sendMsgBuf(cnt*59%256, ext, 8, data);

  if(sndStat == CAN_OK){
    Serial.println("Message Sent Successfully!");
  } else {
    Serial.println("Error Sending Message...");
  }
  delay(50);   // send data per 100ms
}

/*********************************************************************************************************
  END FILE
*********************************************************************************************************/
