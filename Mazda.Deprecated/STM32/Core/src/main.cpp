/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "i2c.h"
#include "spi.h"
#include "usart.h"
#include "usb_device.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "usbd_cdc_if.h"
#include "../../libraries/PT2258/PT2258.h"
#include "../../libraries/MCP_CAN/mcp_can.h"

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
#include <stdarg.h>
#include <string.h>

void vprint(const char *fmt, va_list argp)
{
    char string[200];
    if(0 < vsprintf(string,fmt,argp)) // build string
    {
        HAL_UART_Transmit(&huart2, (uint8_t*)string, strlen(string), 0xffffff); // send message via UART, dopełnienie bitów
		CDC_Transmit_FS((uint8_t*)string, strlen(string));

    }
}

void Printf(const char *fmt, ...) // custom printf() function
{
    va_list argp;
    va_start(argp, fmt);
    vprint(fmt, argp);
    va_end(argp);
}
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_I2C2_Init();
  MX_USART2_UART_Init();
  MX_USB_DEVICE_Init();
  MX_SPI1_Init();
  /* USER CODE BEGIN 2 */
  for (int i = 0; i < 5; i++)
  {
  	Printf("%d\n", i);
  	HAL_Delay(1000);
  }

  Printf("Skan i2c na uart 2 oraz usb cdc");
  Printf("\r\n");


  Printf("Scanning I2C bus:\r\n");
  	HAL_StatusTypeDef result;

   	uint8_t FoundAddr = 0;

   	uint8_t i;
   	for (i=1; i<128; i++)
   	{
   	  /*
   	   * the HAL wants a left aligned i2c address
   	   * &hi2c1 is the handle
   	   * (uint16_t)(i<<1) is the i2c address left aligned
   	   * retries 2
   	   * timeout 2
   	   */
   	  result = HAL_I2C_IsDeviceReady(&hi2c2, (uint16_t)(i<<1), 2, 2);
   	  if (result != HAL_OK) // HAL_ERROR or HAL_BUSY or HAL_TIMEOUT
   	  {
   		Printf("."); // No ACK received at that address
   	  }
   	  if (result == HAL_OK)
   	  {
   		Printf("\r\nDEC: %d\r\n", i); // Received an ACK at that address
   		FoundAddr = i;
   	  }
   	}
   	Printf("\r\nend\r\n");


   	PT2258 a;
   	a.init(FoundAddr);

   	a.setMute(0);
   	a.setMasterVolume(70);
   	a.setChannelVolume(70, 1);




   	MCP_CAN CAN(CAN_CS_GPIO_Port, CAN_CS_Pin);

    if (CAN.begin(MCP_ANY, CAN_500KBPS, MCP_8MHZ) == CAN_OK)
       Printf("MCP2515 Initialized Successfully!");
     else
    	 Printf("Error Initializing MCP2515...");

     CAN.setMode(MCP_NORMAL); // Set operation mode to normal so the MCP2515 sends acks to received data.

     //pinMode(CAN0_INT, INPUT); // Configuring pin for /INT input

     Printf("MCP2515 Library Receive Example...");

     long unsigned int rxId;
     unsigned char len = 0;
     unsigned char rxBuf[8];
     char msgString[128];




  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
     while (1)
     {
       /* USER CODE END WHILE */

       /* USER CODE BEGIN 3 */
//       	  HAL_GPIO_TogglePin(led_GPIO_Port, led_Pin);
       //	  HAL_Delay(1000);
      // Printf("+");
       if (HAL_GPIO_ReadPin(CAN_INT_GPIO_Port, CAN_INT_Pin) == GPIO_PIN_RESET)
       {
        	  HAL_GPIO_TogglePin(led_GPIO_Port, led_Pin);

         CAN.readMsgBuf(&rxId, &len, rxBuf); // Read data: len = data length, buf = data byte(s)

         if ((rxId & 0x80000000) == 0x80000000) // Determine if ID is standard (11 bits) or extended (29 bits)
         {
           sprintf(msgString, "\nE;0x%.8lX;%1d;", (rxId & 0x1FFFFFFF), len);
         }
         else
         {
           sprintf(msgString, "\nS;0x%.3lX;%1d;", rxId, len);
         }

         Printf(msgString);

         if ((rxId & 0x40000000) == 0x40000000)
         { // Determine if message is a remote request frame.
           sprintf(msgString, " REMOTE REQUEST FRAME");
           Printf(msgString);
         }
         else
         {
           for (uint8_t i = 0; i < len; i++)
           {
            // sprintf(msgString, "%.2X", rxBuf[i]);
          //   Printf(msgString);
        	   Printf("%.2X ", rxBuf[i]);
           }
         }
       }
     }
     /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Configure the main internal regulator output voltage
  */
  __HAL_RCC_PWR_CLK_ENABLE();
  __HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);
  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLM = 25;
  RCC_OscInitStruct.PLL.PLLN = 144;
  RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
  RCC_OscInitStruct.PLL.PLLQ = 3;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }
  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
