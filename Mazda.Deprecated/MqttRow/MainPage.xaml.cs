﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.Devices.Enumeration;
using Windows.Devices.SerialCommunication;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.Storage.Streams;
using System.Threading;
using System.Text;
using Windows.UI.Core;
using System.Globalization;
using System.Diagnostics;


// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace MqttRow
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            ListAvailablePorts();

            SetCOM();
        }

        private async Task SetCOM()
        {
            ContentDialog dialog = new ContentDialog();
            dialog.Title = "Wybierz deva";
            dialog.PrimaryButtonText = "1";
            dialog.SecondaryButtonText = "2";
            dialog.CloseButtonText = "0";
            dialog.DefaultButton = ContentDialogButton.None;
            dialog.Content = new ContentDialogContent();

            var result = await dialog.ShowAsync();
            await ConnectToUart((int)result);
        }

        private DeviceInformationCollection listOfDevices;

        private async void ListAvailablePorts()
        {
            try
            {
                string aqs = SerialDevice.GetDeviceSelector();
                listOfDevices = await DeviceInformation.FindAllAsync(aqs);

                TextBlock.Text = listOfDevices.Count + " devices";

            }
            catch (Exception ex)
            {
                TextBlock.Text = ex.Message;
            }
        }

        private SerialDevice serialPort = null;
        private DataReader dataReaderObject = null;
        private async Task ConnectToUart(int portnr)
        {
            if (portnr >= listOfDevices.Count)
            {
                portnr = listOfDevices.Count - 1;
            }

            if (listOfDevices.Count <= 0)
            {
                TextBlock.Text = "Select a device and connect";
                return;
            }

            DeviceInformation entry = listOfDevices[portnr];

            try
            {
                //ConfigSerialPort(entry);
                serialPort = await SerialDevice.FromIdAsync(entry.Id);
                if (serialPort == null)
                    return;


                // Configure serial settings
                ConfigSerialPort(serialPort);

                // Display configured settings
                TextBlock.Text = "Serial port configured successfully: ";
                TextBlock.Text += serialPort.BaudRate + "-";
                TextBlock.Text += serialPort.DataBits + "-";
                TextBlock.Text += serialPort.Parity.ToString() + "-";
                TextBlock.Text += serialPort.StopBits;

                Listen();

            }
            catch (Exception ex)
            {
                TextBlock.Text = ex.Message;
            }
        }

        private void ConfigSerialPort(SerialDevice serialPort)
        {
            if (serialPort == null)
                return;

            // Configure serial settings
            serialPort.WriteTimeout = TimeSpan.FromMilliseconds(100);
            serialPort.ReadTimeout = TimeSpan.FromMilliseconds(60);
            serialPort.BaudRate = 115200;
            serialPort.Parity = SerialParity.None;
            serialPort.StopBits = SerialStopBitCount.One;
            serialPort.DataBits = 8;
            serialPort.Handshake = SerialHandshake.None;
        }

        Dictionary<string, string> UiLines = new Dictionary<string, string>();

        private void AddItem(StringBuilder OneLine)
        {
            string cannrstr = OneLine.ToString(4, 2);

            if (UiLines.ContainsKey(cannrstr))
            {
                UiLines[cannrstr] = OneLine.ToString();
            }
            else
            {
                UiLines.Add(cannrstr, OneLine.ToString());
            }
            tb.Text = string.Join("\n", UiLines.Values);

        }
        private async void Listen()
        {
            try
            {
                int Readed = 0;
                var st = new Stopwatch();

                if (serialPort != null)
                {
                    //ConfigSerialPort(serialPort);

                    dataReaderObject = new DataReader(serialPort.InputStream);
                    dataReaderObject.InputStreamOptions = InputStreamOptions.None;

                    // keep reading the serial input

                    var LineSb = new StringBuilder();
                    st.Start();

                    while (true)
                    {
                        await dataReaderObject.LoadAsync(1);
                        
                        if(dataReaderObject.UnconsumedBufferLength == 0)
                        {
                            continue;
                        }

                        var str = dataReaderObject.ReadString(1);
                        LineSb.Append(str);

                        if (str != "\n")
                        {
                            continue;
                        }

                        if (!LineSb.ToString().Contains("CAN"))
                        {
                            LineSb.Clear();
                            continue;
                        }

                        Readed++;
                        if (Readed > 100)
                        {
                            st.Stop();
                            Debug.WriteLine($"Readed {Readed} in {st.ElapsedMilliseconds} ms");
                            st = Stopwatch.StartNew();
                            Readed = 0;
                        }

                        AddItem(LineSb);
                        LineSb.Clear();
                    }
                }
            }
            catch (TaskCanceledException tce)
            {
                TextBlock.Text = "Reading task was cancelled, closing device and cleaning up";
            }
            catch (Exception ex)
            {
                TextBlock.Text = ex.Message;
            }
            finally
            {
                // Cleanup once complete
                if (dataReaderObject != null)
                {
                    dataReaderObject.DetachStream();
                    dataReaderObject = null;
                }
            }
        }

        private async Task<string> ReadAsync(CancellationToken cancellationToken)
        {
            //Task<UInt32> loadAsyncTask;

            uint ReadBufferLength = 200;

            // If task cancellation was requested, comply
            cancellationToken.ThrowIfCancellationRequested();

            // Set InputStreamOptions to complete the asynchronous read operation when one or more bytes is available
            dataReaderObject.InputStreamOptions = InputStreamOptions.None;


            using (var childCancellationTokenSource = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken))
            {
                // Create a task object to wait for data on the serialPort.InputStream
                //loadAsyncTask = dataReaderObject.LoadAsync(ReadBufferLength).AsTask(childCancellationTokenSource.Token);
                var bytesRead = await dataReaderObject.LoadAsync(ReadBufferLength);

                // Launch the task and wait
                //UInt32 bytesRead = await loadAsyncTask;               
                if (bytesRead > 0)
                {
                    // 1. Read by byte
                    //var _byteToRead = new byte[bytesRead];
                    //dataReaderObject.ReadBytes(_byteToRead);
                    //rcvdText.Text = _byteToRead.ToString();

                    // 2. Read by string                   
                    var ReadVal = dataReaderObject.ReadString(bytesRead);

                    return ReadVal;
                    //status.Text = "bytes read successfully!";
                }
            }
            return string.Empty;
        }
    }

}