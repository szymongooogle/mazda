
#include "AppTasks.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include "usart.h"

App_AllocStaticSemaphore(DMA_Bluetooth_CollectionCompletedSemaphore)

#define MAXBM64CHARS 200

extern UART_HandleTypeDef huart2;

uint16_t Size = 0;
uint8_t RxBM64Buffer[MAXBM64CHARS];
uint8_t BM64CurrentMessage[MAXBM64CHARS];

char BM64CurrentMessageChar[MAXBM64CHARS * 2];

static uint8_t value;

void BluetoothTaskPreparation()
{
    App_PrepareStaticSemaphore(DMA_Bluetooth_CollectionCompletedSemaphore);

    // HAL_UARTEx_ReceiveToIdle_DMA(&huart2, RxBM64Buffer, 100);
    HAL_UART_Receive_IT(&huart2, &value, 1);
}

uint8_t lastLen = 0;

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{

    portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;

    xSemaphoreGiveFromISR(DMA_Bluetooth_CollectionCompletedSemaphore, &xHigherPriorityTaskWoken);

    RxBM64Buffer[Size] = value;
    Size = Size + 1;

    HAL_UART_Transmit(&huart2, &value, 1, 100);
    // SEGGER_SYSVIEW_Print(value);
    HAL_UART_Receive_IT(&huart2, &value, 1);

    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

void ParseMessage();
void BluetoothTaskFunction(void *arg)
{
    while (true)
    {
        xSemaphoreTake(DMA_Bluetooth_CollectionCompletedSemaphore, portMAX_DELAY);
        ParseMessage();
    }
}

uint8_t BluetoothIncomingBuff[20];
void BluetoothIncomingFunction(const char *data, uint16_t len)
{
    static uint8_t FinalLen = 0;

    FinalLen = (uint8_t)len / 2;

    static char TmpBuff[2];

    for (int i = 0; i < FinalLen; i++)
    {
        TmpBuff[0] = data[i * 2];
        TmpBuff[1] = data[i * 2 + 1];

        long hm = strtol((const char *)&TmpBuff, NULL, 16);

        BluetoothIncomingBuff[i] = (uint8_t)hm;
    }

    HAL_UART_Transmit_IT(&huart2, BluetoothIncomingBuff, FinalLen);
}

void HAL_UARTEx_RxEventCallback(UART_HandleTypeDef *huart, uint16_t size)
{
    portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;

    if (huart->Instance != USART2)
    {
        return;
    }
    Size = size;
    xSemaphoreGiveFromISR(DMA_Bluetooth_CollectionCompletedSemaphore, &xHigherPriorityTaskWoken);

    HAL_UARTEx_ReceiveToIdle_DMA(&huart2, RxBM64Buffer, 100);

    // SEGGER_SYSVIEW_RecordExitISR();
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

void ParseMessage()
{
    printf("\n------\n");

    SEGGER_SYSVIEW_Print((const char *)RxBM64Buffer);

    for (int i = 0; i < Size; i++)
    {
        printf(" Ox%x", RxBM64Buffer[i] & 0xff);
    }
    printf("\n");
    fflush(stdout);

    for (uint16_t i = 0; i < Size - 5; i++)
    {
        // Validate start frame. Should be AA, 00, Len, ...message, Checksum
        if (RxBM64Buffer[i] == 0xAA && RxBM64Buffer[i + 1] == 0x00)
        {
            const uint8_t length = RxBM64Buffer[i + 2];
            const uint8_t checksumPos = i + length + 3;
            if (checksumPos > Size)
            {
                printf("checksum > Size");
                fflush(stdout);
                break;
            }
            Size = 0;
            printf("Len: %d\t", length);

            memcpy(BM64CurrentMessage, &RxBM64Buffer[i + 3], length);
            for (int j = 0; j < length; j++)
            {
                printf(" Ox%x", BM64CurrentMessage[j] & 0xff);
                sprintf(&BM64CurrentMessageChar[j * 2], "%02X", BM64CurrentMessage[j]);
            }

            printf("\t chck: %d", RxBM64Buffer[checksumPos]);
            printf("\n------\n");

            static char str[20] = "oBM64/";
            str[6] = BM64CurrentMessageChar[0];
            str[7] = BM64CurrentMessageChar[1];

            AppSenderFunction(str, &BM64CurrentMessageChar[2], length * 2 - 2);
            break;
        }
    }
    //???!!!
    // HAL_UARTEx_ReceiveToIdle_DMA(&huart2, RxBM64Buffer, 100);
}
