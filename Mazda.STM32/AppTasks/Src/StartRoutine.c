#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "main.h"
#include "AppTasks.h"
#include "FreeRTOS.h"
#include "semphr.h"

#define CLICKS 3

void StartRoutineTaskFunction(void* arg)
{
  vTaskDelay(20000 / portTICK_PERIOD_MS);

  for (short i = 0; i < CLICKS; i++)
  {
    vTaskDelay(700 / portTICK_PERIOD_MS);
    LCDIncomingFunction("0", 1);
  }

  vTaskDelay(portMAX_DELAY);
}
