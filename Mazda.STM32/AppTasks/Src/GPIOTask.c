#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include "main.h"
#include "AppTasks.h"
#include "FreeRTOS.h"
#include "semphr.h"
#include <stdbool.h>

#define QUEUE_LEN 5
static QueueHandle_t GpioCmdQueue = NULL;
App_AllocStaticQueue(GpioCmdQueue, 5, MQTTIncomingMessage_t)

struct gpio_pin_mapping_s
{
  GPIO_TypeDef *Port;
  uint16_t PinNumber;
};
#define GPIO_PIN_MAPING_LENGTH 4
static struct gpio_pin_mapping_s GPIO_PIN_MAPING[GPIO_PIN_MAPING_LENGTH] = {
    {MCU_GPIO1_GPIO_Port, MCU_GPIO1_Pin},
    {MCU_GPIO2_GPIO_Port, MCU_GPIO2_Pin},
    {MCU_GPIO3_GPIO_Port, MCU_GPIO3_Pin},
    {MCU_GPIO4_GPIO_Port, MCU_GPIO4_Pin},
};

void GPIOTaskPreparation()
{
  App_PrepareStaticQueue(GpioCmdQueue);
}

void GPIOParseMessage(const char *data, uint16_t len);

void GPIOTaskFunction(void *arg)
{
  MQTTIncomingMessage_t message;
  while (true)
  {
    if (xQueueReceive(GpioCmdQueue, &message, portMAX_DELAY) == pdTRUE)
    {
      GPIOParseMessage(message.data, message.len);
    }
  }
}

void GPIOIncomingFunction(const char *data, uint16_t len)
{
  MQTTIncomingMessage_t message;
  message.data = data;
  message.len = len;

  xQueueSend(GpioCmdQueue, &message, portMAX_DELAY);
}



void SetGPIO(int *pinNr, GPIO_PinState state)
{
  HAL_GPIO_WritePin(GPIO_PIN_MAPING[*pinNr].Port, GPIO_PIN_MAPING[*pinNr].PinNumber, state);

  const int currentState = HAL_GPIO_ReadPin(GPIO_PIN_MAPING[*pinNr].Port, GPIO_PIN_MAPING[*pinNr].PinNumber) == GPIO_PIN_RESET ? 1 : 0;

  char topic[] = "oGPIO/x";
  topic[6] = *pinNr + '0';

  char msg[] = {currentState + '0'};
  // strlen(msg) not work
  AppSenderFunction(topic, msg, 1);
}

void GPIOParseMessage(const char *data, uint16_t len)
{
  if (len != 2)
  {
    printf("SetGPIO.c\tBad request");
    return;
  }

  int pinNr = (int)*data - 48;
  int on = (int)*(data + 1) - 48;

  if (!(0 <= pinNr && pinNr < GPIO_PIN_MAPING_LENGTH))
  {
    printf("SetGPIO.c\tBad pin number");
    return;
  }

  on == 0 ? SetGPIO(&pinNr, GPIO_PIN_SET) : SetGPIO(&pinNr, GPIO_PIN_RESET);
}
