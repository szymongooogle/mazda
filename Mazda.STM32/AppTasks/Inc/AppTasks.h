
#ifndef __APP_TASKS_H
#define __APP_TASKS_H

#include "stdio.h"

#ifdef __cplusplus
extern "C" {
#endif

#include "FreeRTOS.h"
#include "task.h"
#include "semphr.h"

#define App_AllocStaticQueue(var_name, size, type) \
    int var_name##Len = size;                      \
    int var_name##Sizeof_type = sizeof(type);      \
    static QueueHandle_t var_name;                 \
    static StaticQueue_t var_name##Static;         \
    static uint8_t var_name##StorageArea[size * sizeof(type)];

#define App_PrepareStaticQueue(var_name) \
var_name = xQueueCreateStatic(var_name##Len, var_name##Sizeof_type, var_name##StorageArea, &var_name##Static); \
assert_param(var_name != NULL);

#define App_AllocStaticSemaphore(var_name)  \
 static SemaphoreHandle_t var_name = NULL;  \
 static StaticSemaphore_t var_name##Buffer; \

#define App_PrepareStaticSemaphore(var_name)                    \
var_name = xSemaphoreCreateBinaryStatic( &var_name##Buffer );   \
assert_param(var_name != NULL)                                  \

//MQTT Incoming Mesage
typedef struct
{
    const char *data;
    uint16_t len;
} MQTTIncomingMessage_t;

// MQTT Callback
void AppSenderFunction(const char *topic, const char *payload, uint16_t payload_length);

// New task model

void SWCTaskPreparation();
void SWCTaskFunction(void* arg);

void GPIOTaskPreparation();
void GPIOTaskFunction(void* arg);
void GPIOIncomingFunction(const char *data, uint16_t len);

void LCDTaskPreparation();
void LCDTaskFunction(void* arg);
void LCDIncomingFunction(const char *data, uint16_t len);

void VolumeControllerTaskPreparation();
void VolumeControllerTaskFunction(void *arg);
void VolumeControllerIncomingFunction(const char *data, uint16_t len);

void CANTaskPreparation();
void CANTaskFunction(void* arg);
//void CANIncomingFunction(const char *data, uint16_t len);

void BluetoothTaskPreparation();
void BluetoothTaskFunction(void* arg);
void BluetoothIncomingFunction(const char *data, uint16_t len);

void KeyboardTaskPreparation();
void KeyboardTaskFunction(void* arg);
void KeyboardIncomingFunction(const char *data, uint16_t len);

void CPUTemperatureTaskPreparation();
void CPUTemperatureTaskFunction(void* arg);
void CPUTemperatureIncomingFunction(const char *data, uint16_t len);

void MqttBenchyTaskPreparation();
void MqttBenchyTaskFunction(void* arg);
void MqttBenchyIncomingFunction(const char *data, uint16_t len);

void StartRoutineTaskFunction(void* arg);

#ifdef __cplusplus
}
#endif

#endif