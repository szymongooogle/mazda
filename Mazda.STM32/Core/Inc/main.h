/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.h
  * @brief          : Header for main.c file.
  *                   This file contains the common defines of the application.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2021 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H

#ifdef __cplusplus
extern "C" {
#endif

/* Includes ------------------------------------------------------------------*/
#include "stm32f7xx_hal.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
//#include <stdbool.h>
/* USER CODE END Includes */

/* Exported types ------------------------------------------------------------*/
/* USER CODE BEGIN ET */

/* USER CODE END ET */

/* Exported constants --------------------------------------------------------*/
/* USER CODE BEGIN EC */

/* USER CODE END EC */

/* Exported macro ------------------------------------------------------------*/
/* USER CODE BEGIN EM */
#undef LWIP_PROVIDE_ERRNO
/* USER CODE END EM */

/* Exported functions prototypes ---------------------------------------------*/
void Error_Handler(void);

/* USER CODE BEGIN EFP */

/* USER CODE END EFP */

/* Private defines -----------------------------------------------------------*/
#define MQTT_OUTPUT_RINGBUF_SIZE 2048
#define MQTT_REQ_MAX_IN_FLIGHT 255
#define MQTT_CYCLIC_TIMER_INTERVAL 1
#define USER_Btn_Pin GPIO_PIN_13
#define USER_Btn_GPIO_Port GPIOC
#define MCO_Pin GPIO_PIN_0
#define MCO_GPIO_Port GPIOH
#define RMII_MDC_Pin GPIO_PIN_1
#define RMII_MDC_GPIO_Port GPIOC
#define MCU_ADCIN_Pin GPIO_PIN_0
#define MCU_ADCIN_GPIO_Port GPIOA
#define RMII_REF_CLK_Pin GPIO_PIN_1
#define RMII_REF_CLK_GPIO_Port GPIOA
#define RMII_MDIO_Pin GPIO_PIN_2
#define RMII_MDIO_GPIO_Port GPIOA
#define MCU_BT_UART_Rx_Pin GPIO_PIN_3
#define MCU_BT_UART_Rx_GPIO_Port GPIOA
#define RMII_CRS_DV_Pin GPIO_PIN_7
#define RMII_CRS_DV_GPIO_Port GPIOA
#define RMII_RXD0_Pin GPIO_PIN_4
#define RMII_RXD0_GPIO_Port GPIOC
#define RMII_RXD1_Pin GPIO_PIN_5
#define RMII_RXD1_GPIO_Port GPIOC
#define LD1_Pin GPIO_PIN_0
#define LD1_GPIO_Port GPIOB
#define MCU_GPIO1_Pin GPIO_PIN_10
#define MCU_GPIO1_GPIO_Port GPIOE
#define MCU_GPIO2_Pin GPIO_PIN_12
#define MCU_GPIO2_GPIO_Port GPIOE
#define MCU_GPIO3_Pin GPIO_PIN_14
#define MCU_GPIO3_GPIO_Port GPIOE
#define MCU_GPIO4_Pin GPIO_PIN_15
#define MCU_GPIO4_GPIO_Port GPIOE
#define MCU_HSCANRX_Pin GPIO_PIN_12
#define MCU_HSCANRX_GPIO_Port GPIOB
#define RMII_TXD1_Pin GPIO_PIN_13
#define RMII_TXD1_GPIO_Port GPIOB
#define RED_LED_Pin GPIO_PIN_14
#define RED_LED_GPIO_Port GPIOB
#define STLK_RX_Pin GPIO_PIN_8
#define STLK_RX_GPIO_Port GPIOD
#define STLK_TX_Pin GPIO_PIN_9
#define STLK_TX_GPIO_Port GPIOD
#define MCU_SCL_Pin GPIO_PIN_12
#define MCU_SCL_GPIO_Port GPIOD
#define MCU_SCLD13_Pin GPIO_PIN_13
#define MCU_SCLD13_GPIO_Port GPIOD
#define USB_PowerSwitchOn_Pin GPIO_PIN_6
#define USB_PowerSwitchOn_GPIO_Port GPIOG
#define USB_OverCurrent_Pin GPIO_PIN_7
#define USB_OverCurrent_GPIO_Port GPIOG
#define MCU_LCD_PIN1_Pin GPIO_PIN_8
#define MCU_LCD_PIN1_GPIO_Port GPIOC
#define MCU_LCD_PIN2_Pin GPIO_PIN_9
#define MCU_LCD_PIN2_GPIO_Port GPIOC
#define USB_SOF_Pin GPIO_PIN_8
#define USB_SOF_GPIO_Port GPIOA
#define USB_VBUS_Pin GPIO_PIN_9
#define USB_VBUS_GPIO_Port GPIOA
#define USB_ID_Pin GPIO_PIN_10
#define USB_ID_GPIO_Port GPIOA
#define USB_DM_Pin GPIO_PIN_11
#define USB_DM_GPIO_Port GPIOA
#define USB_DP_Pin GPIO_PIN_12
#define USB_DP_GPIO_Port GPIOA
#define TMS_Pin GPIO_PIN_13
#define TMS_GPIO_Port GPIOA
#define TCK_Pin GPIO_PIN_14
#define TCK_GPIO_Port GPIOA
#define MCU_LCD_PIN3_Pin GPIO_PIN_10
#define MCU_LCD_PIN3_GPIO_Port GPIOC
#define MCU_LCD_PIN4_Pin GPIO_PIN_11
#define MCU_LCD_PIN4_GPIO_Port GPIOC
#define MCU_LCD_PIN5_Pin GPIO_PIN_12
#define MCU_LCD_PIN5_GPIO_Port GPIOC
#define MCU_BT_UART_Tx_Pin GPIO_PIN_5
#define MCU_BT_UART_Tx_GPIO_Port GPIOD
#define MCU_RST_Pin GPIO_PIN_6
#define MCU_RST_GPIO_Port GPIOD
#define MCU_MFB_Pin GPIO_PIN_7
#define MCU_MFB_GPIO_Port GPIOD
#define RMII_TX_EN_Pin GPIO_PIN_11
#define RMII_TX_EN_GPIO_Port GPIOG
#define RMII_TXD0_Pin GPIO_PIN_13
#define RMII_TXD0_GPIO_Port GPIOG
#define SW0_Pin GPIO_PIN_3
#define SW0_GPIO_Port GPIOB
#define MCU_HSCANTX_Pin GPIO_PIN_6
#define MCU_HSCANTX_GPIO_Port GPIOB
#define LD2_Pin GPIO_PIN_7
#define LD2_GPIO_Port GPIOB
#define MCU_LSCANRX_Pin GPIO_PIN_8
#define MCU_LSCANRX_GPIO_Port GPIOB
#define MCU_LSCANTX_Pin GPIO_PIN_9
#define MCU_LSCANTX_GPIO_Port GPIOB
/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

#ifdef __cplusplus
}
#endif

#endif /* __MAIN_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
