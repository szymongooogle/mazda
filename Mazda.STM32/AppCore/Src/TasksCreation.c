#include "FreeRTOS.h"
#include "task.h"
#include "AppCore.h"
#include "AppTasks.h"

#define OsPriorityNormal 24
#define STACK_SIZE_256 256

static StackType_t ConnectTaskStack[STACK_SIZE_256 * 4];
static StackType_t SWCTaskStack[STACK_SIZE_256];
static StackType_t GPIOTaskStack[STACK_SIZE_256];
static StackType_t LCDTaskStack[STACK_SIZE_256];
static StackType_t VolumeControllerTaskStack[STACK_SIZE_256];
static StackType_t CANTaskStack[STACK_SIZE_256 * 4];
static StackType_t KeyboardTaskStack[STACK_SIZE_256];
static StackType_t BluetoothTaskStack[STACK_SIZE_256];
static StackType_t CPUTemperatureTaskStack[STACK_SIZE_256];
static StackType_t MqttBenchyTaskStack[STACK_SIZE_256];
static StackType_t StartRoutineTaskStack[STACK_SIZE_256/4];

static StaticTask_t ConnectTaskTCB;
static StaticTask_t SWCTaskTCB;
static StaticTask_t GPIOTaskTCB;
static StaticTask_t LCDTaskTCB;
static StaticTask_t VolumeControllerTaskTCB;
static StaticTask_t CANTaskTCB;
static StaticTask_t KeyboardTaskTCB;
static StaticTask_t BluetoothTaskTCB;
static StaticTask_t CPUTemperatureTaskTCB;
static StaticTask_t MqttBenchyTaskTCB;
static StaticTask_t StartRoutineTaskTCB;

struct AppTask_s
{
    const char *const TaskName;
    TaskFunction_t TaskFunction;
    TaskInitFunction_t TaskInitFunction;
    uint16_t TaskStackSize;
    UBaseType_t TaskPriority;
    StackType_t *const TaskStack;
    StaticTask_t *const TaskTCB;
};

#define APPTASKS_COUNT 11
static const struct AppTask_s AppTasks[APPTASKS_COUNT] =
    {
        {"Connect", ConnectTaskFun, NULL, STACK_SIZE_256 * 4, OsPriorityNormal, ConnectTaskStack, &ConnectTaskTCB},
        {"SWC", SWCTaskFunction, SWCTaskPreparation, STACK_SIZE_256, OsPriorityNormal, SWCTaskStack, &SWCTaskTCB},
        {"GPIO", GPIOTaskFunction, GPIOTaskPreparation, STACK_SIZE_256, OsPriorityNormal, GPIOTaskStack, &GPIOTaskTCB},
        {"LCD", LCDTaskFunction, LCDTaskPreparation, STACK_SIZE_256, OsPriorityNormal, LCDTaskStack, &LCDTaskTCB},
        {"VolumeController", VolumeControllerTaskFunction, VolumeControllerTaskPreparation, STACK_SIZE_256, OsPriorityNormal, VolumeControllerTaskStack, &VolumeControllerTaskTCB},
        {"CAN", CANTaskFunction, CANTaskPreparation, STACK_SIZE_256 * 4, OsPriorityNormal, CANTaskStack, &CANTaskTCB},
        {"Keyboard", KeyboardTaskFunction, KeyboardTaskPreparation, STACK_SIZE_256, OsPriorityNormal, KeyboardTaskStack, &KeyboardTaskTCB},
        {"Bluetooth", BluetoothTaskFunction, BluetoothTaskPreparation, STACK_SIZE_256, OsPriorityNormal, BluetoothTaskStack, &BluetoothTaskTCB},
        {"CPUTemperature", CPUTemperatureTaskFunction, CPUTemperatureTaskPreparation, STACK_SIZE_256, OsPriorityNormal, CPUTemperatureTaskStack, &CPUTemperatureTaskTCB},
        {"MqttBenchy", MqttBenchyTaskFunction, MqttBenchyTaskPreparation, STACK_SIZE_256, OsPriorityNormal, MqttBenchyTaskStack, &MqttBenchyTaskTCB},
        {"StartRoutine", StartRoutineTaskFunction, NULL, STACK_SIZE_256/4, OsPriorityNormal, StartRoutineTaskStack, &StartRoutineTaskTCB},
};

void AppCreateTasks()
{
    for (uint8_t i = 0; i < APPTASKS_COUNT; i++)
    {
        if (AppTasks[i].TaskInitFunction != NULL)
        {
            AppTasks[i].TaskInitFunction();
        }
        xTaskCreateStatic(
            AppTasks[i].TaskFunction,
            AppTasks[i].TaskName,
            AppTasks[i].TaskStackSize,
            NULL,
            AppTasks[i].TaskPriority,
            AppTasks[i].TaskStack,
            AppTasks[i].TaskTCB);
    }
}