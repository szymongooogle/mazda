#include "AppCore.h"
#include "AppTasks.h"
#include "FreeRTOS.h"
#include "task.h"

#include "i2c.h"
#include "PT2258.h"

#include <stdio.h>
#include "SEGGER_SYSVIEW.h"
#include "mqtt_priv.h" //mqtt_client_t

extern mqtt_client_t *app_mqtt_client;

void pt2258_send(uint16_t DevAddress, uint8_t *pData, uint16_t Size)
{
  if (HAL_I2C_Master_Transmit(&hi2c4, DevAddress, pData, Size, HAL_MAX_DELAY) != HAL_OK)
  {
    SEGGER_SYSVIEW_PrintfHost("Blad w wysylce do dec: %d\r\n", (DevAddress >> 1));
  }
}

void pt2258_delay(uint16_t miliseconds)
{
    vTaskDelay(miliseconds / portTICK_PERIOD_MS);
}

static int msgCountFIXME = 0;
void AppSenderFunction(const char *topic, const char *payload, uint16_t payload_length)
{
  msgCountFIXME++;
  // I guess tcp ip thread dont have enough time to send 200+ mqtt msg and free buf
  // Task overflow problem
  if(msgCountFIXME > 100)
  {
    vTaskDelay(100 / portTICK_PERIOD_MS);
    msgCountFIXME=0;
  }

  if (app_mqtt_client->conn_state == 3)
  {

    err_t err = mqtt_publish(app_mqtt_client, topic, payload, payload_length, 1, 1, NULL, NULL);
    if (err != ERR_OK)
    {
      SEGGER_SYSVIEW_PrintfHost("Publish err: %d\n", err);
    }
  }
}

void BeforeSystemStartCallback()
{
  SEGGER_SYSVIEW_Conf();
  HAL_NVIC_SetPriorityGrouping(NVIC_PRIORITYGROUP_4); // ensure proper priority grouping for freeRTOS
}