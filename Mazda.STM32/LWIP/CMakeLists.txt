cmake_minimum_required(VERSION 3.22)

#
# List of source files to compile
#
set(LWIPSrc
    # Put here your source files, one in each line, relative to CMakeLists.txt file location
    ${CMAKE_CURRENT_LIST_DIR}/Target/ethernetif.c 
    ${CMAKE_CURRENT_LIST_DIR}/App/lwip.c 
)

#
# Include directories
#
set(LWIPInc
    # Put here your include dirs, one in each line, relative to CMakeLists.txt file location
    ${CMAKE_CURRENT_LIST_DIR}/App 
    ${CMAKE_CURRENT_LIST_DIR}/Target 
)