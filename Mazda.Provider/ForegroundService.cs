﻿using Android.App;
using Android.Content;
using Android.Util;
using Mazda.Shared;
using System;
using System.Threading.Tasks;

namespace Mazda.Provider
{
    [Service(Exported = true, Name = "com.SzymonMusial.mazda.provider.KeepAliveService")]
    public class ForegroundService : IntentService
    {
        MqttClient mqttClient;
        public ForegroundService() : base("KeepAliveService")
        {
            mqttClient = MqttClient.Instance;
        }
        protected override void OnHandleIntent(Intent intent)
        {
            ShowNotify(1, "Serwis mqtt", "Serwis rozpoczął prace", "Informacje", NotificationImportance.Low, NotificationVisibility.Public);
            ServiceJob().Wait();
            ShowNotify(1, "Serwis mqtt", "Serwis Zakończył prace", "Informacje", NotificationImportance.Low, NotificationVisibility.Public);
        }

        private async Task ServiceJob()
        {
            mqttClient.Subscribe("GoogleMapsAdres", GoogleMapsAdresCallBack);
            mqttClient.Subscribe("WazeMapsAdres", WazeAdresCallBack);
            Read_x86_pkg_temp();
            await Task.Run(() => Task.Delay(int.MaxValue));
        }

        private void GoogleMapsAdresCallBack(string data)
        {
            var geoUri = Android.Net.Uri.Parse("google.navigation:q=" + data);
            var mapIntent = new Intent(Intent.ActionView, geoUri);
            StartActivity(mapIntent);
        }

        private void WazeAdresCallBack(string data)
        {            
            var geoUri = Android.Net.Uri.Parse("https://waze.com/ul?q=" + data + "&navigate=yes");
            var mapIntent = new Intent(Intent.ActionView, geoUri);
            StartActivity(mapIntent);
        }

        /// <summary>
        /// Funkcja pokazująca powiadomienia
        /// </summary>
        /// <param name="NotifyID">ID powiadomienia</param>
        /// <param name="NotifyTitle">Tytuł powiadomienia</param>
        /// <param name="Notifytext">Zawartość<param>
        /// <param name="ChannelName">Nazwa kanału powiadomień</param>
        /// <param name="notificationImportance">Ważność powiadomienia</param>
        /// <param name="notificationVisibility">Widoczność dla innych powiadomienia</param>
        static public void ShowNotify(int NotifyID, string NotifyTitle, string NotifyText, string ChannelName, NotificationImportance notificationImportance, NotificationVisibility notificationVisibility)
        {
            string URGENT_CHANNEL = ChannelName;
            NotificationManager notificationManager = (NotificationManager)Application.Context.GetSystemService(NotificationService);


            // Work has finished, now dispatch anotification to let the user know.
            Notification.Builder notificationBuilder = new Notification.Builder(Application.Context)
                .SetSmallIcon(Resource.Mipmap.ic_launcher)
                .SetContentTitle(NotifyTitle)
                .SetContentText(NotifyText);


            try
            {
                // Avaliable in android 8.0
                NotificationChannel chan = new NotificationChannel(URGENT_CHANNEL, ChannelName, notificationImportance);
                chan.EnableVibration(true);
                chan.LockscreenVisibility = notificationVisibility;
                notificationManager.CreateNotificationChannel(chan);
                notificationBuilder.SetChannelId(URGENT_CHANNEL);
            }
            catch { }

            //var notificationManager = (NotificationManager)GetSystemService(NotificationService);
            notificationManager.Notify(NotifyID, notificationBuilder.Build());

        }
        // cat /sys/class/thermal/thermal_zone*/temp temperatuty
        // cat /sys/class/thermal/thermal_zone*/type nazwy sensorów
        // https://askubuntu.com/questions/1110943/what-do-the-different-thermal-zones-actually-correspond-to
        public async void Read_x86_pkg_temp()
        {
            try
            {
                while (true)
                {                  
                    string file = "/sys/class/thermal/thermal_zone0/temp";
                    using (var reader = new System.IO.StreamReader(file, true))
                    {
                        string line;
                        line = await reader.ReadLineAsync();
                        if (!string.IsNullOrEmpty(line))
                        {
                            var doubleLine = double.Parse(line);
                            doubleLine = doubleLine * 0.001;
                            mqttClient.Send("x86_pkg_temp", doubleLine.ToString());
                        }
                    }
                    await Task.Delay(1000);
                }
            }
            catch (Exception e)
            {
                Log.Error("Mazda Foreground", e.Message);
            }
        }

    }
}