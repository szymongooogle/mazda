﻿using Android.OS;
using Android.Views;
using AndroidX.Fragment.App;
using System;

namespace Mazda.Shared.AppFragmentMgr
{
    public class DefaultFragmentInflater : Fragment
    {
        public int ResourceId { get; private set; }

        /// <summary>
        /// Pass layout view id to inflate
        /// </summary>
        /// <param name="resourceId">Resource.Layout resource id</param>
        public DefaultFragmentInflater(int resourceId) => ResourceId = resourceId;

        /// <summary>
        /// Being explicit about the requirement for a default constructor.
        /// In this case ResourceId will be got from bundle
        /// </summary>
        public DefaultFragmentInflater() { }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            if (savedInstanceState != null)
            {
                ResourceId = savedInstanceState.GetInt(nameof(ResourceId), -1);
                if (ResourceId == -1)
                {
                    throw new ArgumentNullException(nameof(ResourceId));
                }
            }
        }

        public override void OnSaveInstanceState(Bundle outState)
        {
            base.OnSaveInstanceState(outState);
            outState.PutInt(nameof(ResourceId), ResourceId);
        }

        public override View OnCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
            => inflater.Inflate(ResourceId, container, false);
    }
}