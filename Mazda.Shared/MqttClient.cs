﻿using MQTTnet;
using MQTTnet.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Mazda.Shared
{
    public class MqttClient
    {
        public static MqttClient Instance { get; private set; } = new MqttClient();
        private readonly MqttFactory factory = new MqttFactory();
        private readonly IMqttClient mqttClient;
        private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private readonly Task ConnectToServerAsyncTask;
        /// <summary>
        /// Słownik zawierający pary temat - kilka akcji z argumentem wiadomości typu string
        /// </summary>
        private static readonly List<KeyValuePair<string, Action<string>>> Subscriptions = new List<KeyValuePair<string, Action<string>>>();  // thread safety
        private static string MqttSerwer = "127.0.0.1";

        /// <summary>
        /// Lista opóźnionych subskrybcji
        /// </summary>
        readonly List<KeyValuePair<string, Action<string>>> TooFastSubscribe = new List<KeyValuePair<string, Action<string>>>();
        public bool IsConnected => mqttClient.IsConnected;

        public delegate void Notify(string message);
        public event Notify NotifyEvent;

        public delegate void ConnectionStateChanged(bool newState);
        public event ConnectionStateChanged ConnectionStateChangedEvent;

        // run before 1st call
        public static void ConfigureMqttClient(string mqttSerwer = "127.0.0.1")
        {
            MqttSerwer = mqttSerwer;
            // fix me
            Instance = new MqttClient();
        }

        private MqttClient()
        {
            mqttClient = factory.CreateMqttClient();
            ConnectToServerAsyncTask = new Task(ConnectToServerAsync, cancellationTokenSource.Token);
            ConnectToServerAsyncTask.Start();
        }

        private async void ConnectToServerAsync()
        {
            var options = new MqttClientOptionsBuilder()
                .WithTcpServer(MqttSerwer, 1883)
                .WithKeepAlivePeriod(TimeSpan.FromSeconds(3))
                .WithWillQualityOfServiceLevel(MQTTnet.Protocol.MqttQualityOfServiceLevel.AtMostOnce)
                .WithTimeout(TimeSpan.FromSeconds(3))
                .Build();

            mqttClient.ConnectedAsync += ConnectedHandler;
            mqttClient.DisconnectedAsync += DisconnectedHandler;
            mqttClient.ApplicationMessageReceivedAsync += MessageReceived;

            try
            {
                var res = await mqttClient.ConnectAsync(options, cancellationTokenSource.Token);
                NotifyEvent?.Invoke($"MQTT Connect ok {res}");
            }
            catch (Exception ex)
            {
                NotifyEvent?.Invoke($"MQTT Connect error {ex.Message}");
            }
            finally
            {
                ConnectionStateChangedEvent?.Invoke(IsConnected);
            }
        }

        private Task ConnectedHandler(MqttClientConnectedEventArgs e)
            => Task.Run(async () =>
            {
                foreach (var i in TooFastSubscribe)
                {
                    await mqttClient.SubscribeAsync(i.Key);
                    Subscriptions.Add(i);
                }

                lock (TooFastSubscribe)
                {
                    NotifyEvent?.Invoke($"MQTT Coonnected");
                    //NotifyEvent?.Invoke($"MQTT Coonnected. Subscribed too fast {TooFastSubscribe.Count}");
                    TooFastSubscribe.Clear();
                }
            });


        private Task DisconnectedHandler(MqttClientDisconnectedEventArgs arg)
            => Task.Run(() =>
            {
                ConnectionStateChangedEvent?.Invoke(IsConnected);
                NotifyEvent?.Invoke("MQTT Disconnected");
            });

        public Task MessageReceived(MqttApplicationMessageReceivedEventArgs e)
            => Task.Run(() =>
            {
                foreach (var sub in Subscriptions.Where(sub => sub.Key == e.ApplicationMessage.Topic))
                {
                    sub.Value((Encoding.UTF8.GetString(e.ApplicationMessage.Payload)));
                }
            });


        public void Subscribe(string TopicName, Action<string> callback) => Task.Run(async () => await SubscribeAsync(TopicName, callback));
        public async Task<KeyValuePair<string, Action<string>>> SubscribeAsync(string TopicName, Action<string> callback)
        {
            var ItemToAdd = new KeyValuePair<string, Action<string>>(TopicName, callback);
            // połaczy sie zaraz
            if (!mqttClient.IsConnected)
            {
                TooFastSubscribe.Add(ItemToAdd);
            }
            else
            {
                // ok, dodaj
                await mqttClient.SubscribeAsync(TopicName);
                Subscriptions.Add(ItemToAdd);
            }
            return ItemToAdd;
        }

        public void Send(string Topic, string Message) => Task.Run(async () => await SendAsync(Topic, Message, cancellationTokenSource.Token));

        private async Task SendAsync(string Topic, string Message, CancellationToken ct)
        {
            if (!mqttClient.IsConnected)
            {
                return;
            }
            var MessageBuilder = new MqttApplicationMessageBuilder()
                .WithTopic(Topic)
                .WithPayload(Message)
                .Build();
            await mqttClient.PublishAsync(MessageBuilder, ct);
        }
        public void RemoveFromSubscriptions(KeyValuePair<string, Action<string>> item)
        {
            Subscriptions.Remove(item);
        }
    }
}