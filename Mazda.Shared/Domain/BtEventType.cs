﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Mazda.Shared.Domain
{
    public partial class BtEventType
    {
        public static Dictionary<int, string> BtEventTypeNames = new Dictionary<int, string>()
        {
            { 0x1A, "AVC_Vendor_Dependent_Response" },
            { 0x1B, "BTM_Utility_Req" },
            { 0x10, "EQ_Mode_Indication" },
            { 0x2D, "Report_Type_Codec" },
            { 0x01, "BTM_Status" },
            { 0x32, "LE_Signaling_Event" }
        };

        public static Dictionary<int, ParamsNamesBase> BtEventTypeNamesHolder = new Dictionary<int, ParamsNamesBase>()
        {
            {0x1B, new BTM_Utility_Req() },
        };
    }

    public partial class BtEventType
    {
        public BtEventType(int eventTypeId, string RawContent)
        {
            var span = RawContent.AsSpan();

            var arr = new List<byte>();

            for (int i = 0; i < RawContent.Length / 2; i++)
            {
                var newIntValue = byte.Parse(span.Slice(i * 2, 2), System.Globalization.NumberStyles.HexNumber);
                arr.Add(newIntValue);
            }

            EventTypeId = eventTypeId;
            Params = arr.ToArray();
        }

        public int EventTypeId { get; private set; }
        public string EventTypeName => BtEventTypeNames.ContainsKey(EventTypeId) ? BtEventTypeNames[EventTypeId] : EventTypeId.ToString();
        public byte[] Params { get; private set; }
        public ParamsNamesBase ParamsNames { get; private set; }

        public string ParamsName
        {
            get
            {
                if (!BtEventTypeNamesHolder.ContainsKey(EventTypeId))
                {
                    return $"E:{EventTypeId} {string.Join(" ", Params)}";
                }

                var currentParamsNames = BtEventTypeNamesHolder[EventTypeId];
                var foundTranslation = currentParamsNames.Names.FirstOrDefault(i => i.Key.SequenceEqual(Params));
                return foundTranslation.Value != null ? foundTranslation.Value : $"E:{EventTypeId} {string.Join(" ", Params)}";
            }
        }
    }

    public class ParamsNamesBase
    {
        public virtual Dictionary<byte[], string> Names { get; }
    }

    public class BTM_Utility_Req : ParamsNamesBase
    {
        public override Dictionary<byte[], string> Names => new Dictionary<byte[], string>()
        {
            { new byte[] {0x4, 0x1 }, "A2DP ON" },
            { new byte[] {0x4, 0x0 }, "A2DP OFF" },
        };
    }
}