﻿using Android.Content;
using Android.Content.Res;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using System;
using System.Text;

namespace Mazda.Shared.CustomControls
{
    public class MyCanTextView : CustomControlBase
    {
        #region ctor
        public MyCanTextView(Context context) : base(context)
        {
        }

        public MyCanTextView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public MyCanTextView(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
        }

        public MyCanTextView(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
        {
        }

        protected MyCanTextView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }
        #endregion

        TextView textView;
        int CanBytePosition;
        int CanAndByteMask;
        int CanByteLength;
        float CanValueScale;

        public override void Initialize(Context context, IAttributeSet attrs = null)
        {
            base.Initialize(context, attrs);
            View view = layoutInflater.Inflate(Resource.Layout.mycanviewlayout, this, true);
            textView = view.FindViewById<TextView>(Resource.Id.textview);

            if (attrs != null)
            {
                TypedArray ta = context.ObtainStyledAttributes(attrs, Resource.Styleable.MyCanViewStyleable);
                view.FindViewById<TextView>(Resource.Id.title).Text = CardTitle;

                CanBytePosition = ta.GetInteger(Resource.Styleable.MyCanViewStyleable_CanBytePosition, -1);
                CanAndByteMask = ta.GetInteger(Resource.Styleable.MyCanViewStyleable_CanAndByteMask, 0xFFFFFF);
                CanByteLength = ta.GetInteger(Resource.Styleable.MyCanViewStyleable_CanByteLength, 1);
                CanValueScale = ta.GetFloat(Resource.Styleable.MyCanViewStyleable_CanValueScale, 1.0f);

                ta.Recycle();
            }

        }
        public override void Callback(string payload)
        {
            if (CanBytePosition == -1 || payload.Length < CanBytePosition*2 + CanByteLength * 2)
            {
                activity.RunOnUiThread(() => textView.Text = payload);
                return;
            }

            var spanByteStartIndex = CanBytePosition * 2;
            var span = payload.AsSpan();

            var newHexStringValue = new StringBuilder();
            for (int i = 0; i < CanByteLength; i++)
            {
                newHexStringValue.Append(span[spanByteStartIndex + i * 2]);
                newHexStringValue.Append(span[spanByteStartIndex + i*2 + 1]);
            }

            var newIntValue = int.Parse(newHexStringValue.ToString(), System.Globalization.NumberStyles.HexNumber);

            float newFloatValue = newIntValue & CanAndByteMask;
            newFloatValue = newIntValue * CanValueScale;

            activity.RunOnUiThread(() => textView.Text = newFloatValue.ToString());
        }
    }
}