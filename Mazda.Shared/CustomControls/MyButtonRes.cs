﻿using Android.Content;
using Android.Content.Res;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using System;

namespace Mazda.Shared.CustomControls
{
    internal class MyButtonRes : CustomControlBase
    {
        #region ctor
        protected MyButtonRes(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }

        public MyButtonRes(Context context) : base(context)
        {
        }

        public MyButtonRes(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public MyButtonRes(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
        }

        public MyButtonRes(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
        {
        }
        #endregion
        TextView title, textView;
        Button button;
        string MqttSendMessage = "", MqttSend = "";

        public override void Initialize(Context context, IAttributeSet attrs = null)
        {
            string ButtonTitle = "";
            base.Initialize(context, attrs);
            View view = layoutInflater.Inflate(Resource.Layout.mybuttonreslayout, this, true);

            if (attrs != null)
            {
                TypedArray ta = context.ObtainStyledAttributes(attrs, Resource.Styleable.MyButtonStyleable);
                ButtonTitle = ta.GetString(Resource.Styleable.MyButtonStyleable_ButtonTitle);
                MqttSend = ta.GetString(Resource.Styleable.MyButtonStyleable_MqttSend);
                MqttSendMessage = ta.GetString(Resource.Styleable.MyButtonStyleable_MqttSendMessage);
                textView = view.FindViewById<TextView>(Resource.Id.textview);
                textView.Text = ta.GetString(Resource.Styleable.MyButtonStyleable_DefaultResValue) ?? "...";
                ta.Recycle();
            }

            title = view.FindViewById<TextView>(Resource.Id.title);
            title.Text = CardTitle;

            button = view.FindViewById<Button>(Resource.Id.button);
            button.Text = ButtonTitle;
            button.Click += Button_Click;

        }

        private void Button_Click(object sender, EventArgs e)
        {
            mqttClient.Send(MqttSend, MqttSendMessage);
        }

        public override void Callback(string payload)
        {
            activity.RunOnUiThread(() => textView.Text = payload);
        }
    }
}