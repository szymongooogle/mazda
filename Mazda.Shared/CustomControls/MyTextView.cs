﻿using Android.Content;
using Android.Content.Res;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using System;

namespace Mazda.Shared.CustomControls
{
    public class MyTextView : CustomControlBase
    {
        #region ctor
        public MyTextView(Context context) : base(context)
        {
        }

        public MyTextView(Context context, IAttributeSet attrs) : base(context, attrs)
        {
        }

        public MyTextView(Context context, IAttributeSet attrs, int defStyleAttr) : base(context, attrs, defStyleAttr)
        {
        }

        public MyTextView(Context context, IAttributeSet attrs, int defStyleAttr, int defStyleRes) : base(context, attrs, defStyleAttr, defStyleRes)
        {
        }

        protected MyTextView(IntPtr javaReference, JniHandleOwnership transfer) : base(javaReference, transfer)
        {
        }
        #endregion

        TextView textView;
        public override void Initialize(Context context, IAttributeSet attrs = null)
        {
            base.Initialize(context, attrs);
            View view = layoutInflater.Inflate(Resource.Layout.mytextviewlayout, this, true);
            textView = view.FindViewById<TextView>(Resource.Id.textview);

            if (attrs != null)
            {
                TypedArray ta = context.ObtainStyledAttributes(attrs, Resource.Styleable.CustomControlBaseStyleable);
                view.FindViewById<TextView>(Resource.Id.title).Text = ta.GetString(Resource.Styleable.CustomControlBaseStyleable_CardTitle);
                ta.Recycle();
            }

        }
        public override void Callback(string payload)
        {
            activity.RunOnUiThread(() => textView.Text = payload);
        }
    }
}