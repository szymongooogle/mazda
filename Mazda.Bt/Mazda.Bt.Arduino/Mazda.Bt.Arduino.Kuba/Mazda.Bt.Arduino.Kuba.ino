#include "BM64.h"
#include <SoftwareSerial.h>

bool MusicState = false; 
bool CallState = false;
bool PreviousMusicState = MusicState; 


#define BM64_IND_PIN 9 // not used
#define BM64_RESET_PIN A5
#define BM64_MFB_PIN A4
#define ADCPin A3
#define AUXPin 7


#define SWC_ON_OFF_PIN 8

#define ModeKeyNr 4 // TODO in prod

// dla arduino mega
//HardwareSerial* _swSerial = &Serial1;
//HardwareSerial* _swSerial = &Serial;

// dla atmegi 328 - arduino uno
SoftwareSerial* _swSerial = new SoftwareSerial(2, 3);
#define BM64_DEBUG // Switch debug output on and off by commentout
#define _DEBUG // Kompilacja debug

#define _DEBUG_ADC
#define _LOG_EVENT

BM64 bm64(Serial, BM64_IND_PIN);
//BM64 bm64(Serial1, BM64_IND_PIN);

//domysny czas determinujący długie naciśnięcie
#define LongTimePressTimeThreshold 2000
// debounce short time
#define ShortTimePressTimeThreshold 100

byte LastKeyDownIndex = -1;

#define _DEBUG_ADC_KEY

#ifdef _DEBUG_ADC_KEY
# define PA(x) _swSerial->println(x);
#else
# define PA(x)
#endif

void RelaseSWC()
{
    digitalWrite(SWC_ON_OFF_PIN, LOW);
#ifdef _DEBUG
        _swSerial->println("SWC -");
#endif
}

void HandleSWC()
{
    digitalWrite(SWC_ON_OFF_PIN, HIGH);
    // pętla loop będzie odczytywać wartość ADC i następnie uruchamiać funkcje
#ifdef _DEBUG
        _swSerial->println("SWC +");
#endif
}

void RelaseAUX()
{
    digitalWrite(AUXPin, LOW);
#ifdef _DEBUG
        _swSerial->println("AUX -");
#endif
}

void HandleAUX()
{
    digitalWrite(AUXPin, HIGH);    
#ifdef _DEBUG
        _swSerial->println("AUX +");
#endif
}

void SetMusicState(bool NewState)
{
    if(NewState)
    {
        // music on
        HandleSWC();
        HandleAUX();
        MusicState = true;
    }
    else
    {
        // music ff
        RelaseSWC();
        RelaseAUX();
        MusicState = false;
        bm64.musicControl(MUSIC_CONTROL_STOP);
    }
}

void Execute(const unsigned int &FunctionNumber)
{
#ifdef _DEBUG
    _swSerial->print("Execute nr: ");
    _swSerial->println(FunctionNumber);
#endif

    switch (FunctionNumber)
    {
        case 1:
        {
            bm64.musicControl(MUSIC_CONTROL_NEXT);
            break;
        }
        case 2:
        {
            bm64.musicControl(MUSIC_CONTROL_PREV);
            break;
        }
        case 3:
        {
            bm64.musicControl(MUSIC_CONTROL_TOGGLE);
            break;
        }
        case 4:
        {
            bm64.musicControl(MUSIC_CONTROL_STOP);
            break;
        }
        case 24:
        {            
            HandleSWC();
            break;
        }
        case 25:
        {
            RelaseSWC();
            break;
        }
        case 6:
        {
            // vol up
            bm64.mmiAction(BM64_MMI_INC_SPK_GAIN);
            break;
        }
        case 7:
        {
            // vol down
            bm64.mmiAction(BM64_MMI_DEC_SPK_GAIN);
            break;
        }
        case 8:
        {
            bm64.musicControl(MUSIC_CONTROL_FAST_FORWARD);
            break;
        }
        case 9:
        {
            bm64.musicControl(MUSIC_CONTROL_REWIND);
            break;
        }
        case 10:
        {
            // toogle mic mute
            bm64.mmiAction(BM64_MMI_MIC_MUTE_TOGGLE);
            break;
        }
        case 11:
        {
            // acccept call
            bm64.mmiAction(BM64_MMI_ACCEPT_CALL);
            break;
        }
        case 12:
        {
            // end call
            bm64.mmiAction(BM64_MMI_FORCE_END_CALL);
            break;
        }
        case 13:
        {
            // increase mic
            bm64.mmiAction(BM64_MMI_INC_MIC_GAIN);   
            break;
        }
        case 14:
        {
            // decrease call
            bm64.mmiAction(BM64_MMI_DEC_MIC_GAIN);    
            break;
        }  

        case 21:
        {      
            // set music state on     
            SetMusicState(true);
            break;
        }
        case 22:
        {
            // set music state off      
            SetMusicState(false);
            break;
        }     
        case 23:
        {
            // toggle music state
            MusicState ? SetMusicState(false) : SetMusicState(true);
            break;
        }  

        default:
        {
#ifdef _DEBUG
            _swSerial->println(" not implemented");
#endif
            break;
        }
    }
}

// struktura pojedynczego klawisza
struct Key
{    
    const unsigned short ShortPressFunctionNumber;
    const unsigned short CallPressFunctionNumber;
    // Key(unsigned short int ShortPressFunctionNumber_, unsigned short int CallPressFunctionNumber_):
    //     ShortPressFunctionNumber(ShortPressFunctionNumber_), CallPressFunctionNumber(CallPressFunctionNumber_)
    //     {}
    void Run()    
    {
        if(CallState)
        {
            Execute(CallPressFunctionNumber);
        }
        else
        {
            Execute(ShortPressFunctionNumber);
        }
    }
};



//error: too many initializers for Key -> przy zmianie l. klawiszy
#define KeysLength 8
// !! iteracja od 1
Key Keys[KeysLength]
{
    {7, 7},  // Vol -
    {6, 6},  // Vol +
    {23, 23},  // Mode
    {6, 10},  // Mute

    {2, 12},  // CH down
    {1, 11},  // CH up
    {9, 14},  // Seek rev
    {8, 13},  // Seek plus
};

// czas kiedy oblicza średnią
#define DebounceInterval 50
//#define DebounceInterval 500
// liczba pomiarów
#define DebounceProbes 50
unsigned long int ADCValsSum = 0;
unsigned int ADCValsReaded = 0; 

void onEventCallback(BM64_event_t *event)
{
#ifdef _LOG_EVENT
    // handle Event
    _swSerial->print("[EVENT]: ");
    _swSerial->println(event->event_code, HEX);

    for(int i=0; i<event->param_len; i++){
        _swSerial->print(event->parameter[i], HEX);
        _swSerial->print(" ");
    }
    _swSerial->println("");
#endif
    switch (event->event_code)
    {
    case BM64_EVENT_CALL_STATUS:
    {
        if (event->param_len > 1)
        {
#ifdef _DEBUG
            _swSerial->print("\nCall Event :");
            _swSerial->println(event->parameter[1]);
#endif
            // event->parameter[1] == 0 IDLE
            // event->parameter[1] == 2, 3, 4 - INCOMING_CALL, OUTGOING_CALL, CALL_ACTIVE

            if(event->parameter[1] == 0)
            {
                // call end
                CallState = false;
                if(!MusicState)
                {
                    // prev no music
                    RelaseSWC();
                    RelaseAUX();
                }
            }
            else
            {
                // start call
                //PreviousMusicState = MusicState;
                CallState = true;
                HandleSWC();
                HandleAUX();
            }
        }

        break;
    }

    default:
        break;
    }    
}

void setup()
{
    pinMode(BM64_RESET_PIN, OUTPUT);
    pinMode(BM64_MFB_PIN, OUTPUT);
    Serial.begin(38400);
    _swSerial->begin(115200);
    bm64.setCallback(onEventCallback);
    pinMode(SWC_ON_OFF_PIN, OUTPUT);
    pinMode(AUXPin, OUTPUT);
    pinMode(AUXPin,INPUT_PULLUP); // Problem powolnego zbocza narasającego, kondensatora wewnątrz

#ifdef _DEBUG
        _swSerial->println("Off");
#endif
    digitalWrite(BM64_MFB_PIN, HIGH);
    digitalWrite(BM64_RESET_PIN, HIGH);
    delay(1000);
    digitalWrite(BM64_MFB_PIN, LOW);
    digitalWrite(BM64_RESET_PIN, LOW);
#ifdef _DEBUG
        _swSerial->println("Setup");
#endif
}


unsigned long currentMillis = millis();
unsigned long previousMillis = millis();

unsigned short ADCToKeyNumber(unsigned int *ADCVal)
{
    // Jak wyższe od wartości liczbowej to klik
    if(*ADCVal > 903)
    {
        return 0;
    }
    if(*ADCVal > 700)
    {
        PA("Vol down");
        return 1;
    }
    else if(*ADCVal > 586)
    {
        PA("Vol up");
        return 2;
    }
    else if (*ADCVal > 503)
    {
        PA("Mode");
        return 3;
    }
    else if (*ADCVal > 407)
    {
        PA("Mute");
        return 4;
    }
    else if (*ADCVal > 303)
    {
        PA("Chanel down");
        return 5;
    }
    else if (*ADCVal > 202)
    {
        PA("Chanel up");
        return 6;
    }
    else if (*ADCVal > 108)
    {
        PA("Seek rev");
        return 7;
    }
    else if (*ADCVal > 27)
    {
        PA("Seek for");
        return 8;
    }
    else
    {
        return 0;
    }
    // [1, 2, 3, 4, 5, 6, 7, 8!, 10!]
    //-0.008675 x + 9.616
    //return round(-0.00994 * (*ADCVal) + 8.512);
}

void UpdateKeyState(unsigned int ADCVal)
{
    unsigned short KeyNumber = ADCToKeyNumber(&ADCVal);

#ifdef _DEBUG_ADC
    if (!(KeyNumber < 0 || KeyNumber > (KeysLength)))
    {
        // _swSerial->print("ADC: ");
        // _swSerial->print(ADCVal);
        // _swSerial->print("\tKey number: ");
        // _swSerial->println(KeyNumber);
    }
#endif
    if (KeyNumber != LastKeyDownIndex)
    {
        // zmienił się klawisz

#ifdef _DEBUG
        // _swSerial->print("Key chng: ");
        // _swSerial->println(KeyNumber);
        // _swSerial->print("ADC: ");
        // _swSerial->println(ADCVal);
#endif

        //[ 1 - 10]
        // 1 -> pull up tj nic nie nacisnieto

        // last key down like a KEY UP, KeyNumber will be zero state
        if (1 <= LastKeyDownIndex  && LastKeyDownIndex <= KeysLength)
        {
            if (!MusicState) // if mode (turn on music) pressed
            {
                if (LastKeyDownIndex == ModeKeyNr) // !! mode
                {
                    Keys[(LastKeyDownIndex - 1)].Run(); // Run mode Super key
                }
            }
            else
            {
                // Key nr [1;8]
                //_swSerial->println(LastKeyDownIndex);
                Keys[(LastKeyDownIndex - 1)].Run();
            }
        }
        LastKeyDownIndex = KeyNumber;
    }
}

void loop()
{
    //Check UART Event
    bm64.run();


    unsigned long currentMillis = millis();

    if (currentMillis - previousMillis >= DebounceInterval)
    {
        if(ADCValsReaded >= DebounceProbes)
        {
            UpdateKeyState(ADCValsSum / ADCValsReaded);
            ADCValsSum = ADCValsReaded = 0;
            previousMillis = currentMillis;  
        }
        ADCValsSum += analogRead(ADCPin);
        ADCValsReaded++; 
    }

    if (_swSerial->available() > 0)
    {
        // read the incoming byte

        String incomingString = _swSerial->readStringUntil('\n');
        long result = incomingString.toInt();

        if(result == 0)
        {
            // parsing error
            return;
        }

        Execute(result);

        // read other not important ex: \r
        while(_swSerial->available() > 0)
        {
            char c = Serial.read();
        }
    }
}
