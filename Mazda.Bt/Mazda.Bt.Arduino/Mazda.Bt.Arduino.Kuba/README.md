# Zestaw głośnomówiący kia

System ma umożliwić nawiązywanie, odbieranie połączeń słuchanie muzyki bt wraz ze sterowaniem z kierownicy zintegrowanym z fabrycznym radiem

## Podłącznie
- Fabryczne radio posiada przewód sygnałowy który podciągnięty do masy automatycznie przełącza radio w tryb aux
- Przewód aux, będzie podłączony do modułu bm64
- szeregowo połączona linia swc (sterowanie kierownicy). 
- - W przypadku działania radia lnia swc jest zwarta.
- - W przypadku przychodzącego połączenia telefonicznego linia swc jest rozwierana czego skutkiem jest odbiór przycisków kierownicy tylko przez moduł pozwalając np. odebrać połączenie
- - W przypadku słuchania muzyki z bt linia swc jest rozwierana czego skutkiem jest odbiór przycisków kierownicy tylko przez moduł pozwalając np. przełączać utwory

## Rozwiązanie
- Atmega 328 - odbiór sygnałów z lini swc, wysterowanie bm64, zmiana trybu radia
- bm64 - obsługa połączenia bt
- Separacja galwaniczna - rozwiązanie problemu pętli masy
- Przekaźnik swc - odcinanie / załączanie swc dla radia
- Tranzystor pull-down - przestawianie radia w tryb aux