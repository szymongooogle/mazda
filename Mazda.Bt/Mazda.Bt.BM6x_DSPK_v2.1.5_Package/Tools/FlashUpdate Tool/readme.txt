DSPK v2.x.x package works on BM62/BM63/BM64 modules which contains 
IS2062/IS2063/IS2064 IC's. Flash update tools are different for 
BM62/IS2062 and BM64/63/IS2064/IS2063. Following are the details

isbtflash.exe - BM62/IS2062
isupdate.exe -  BM63/BM64/IS2063/IS2064