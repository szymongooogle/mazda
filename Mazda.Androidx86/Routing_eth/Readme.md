### Przeniesienie plików

```sh
adb push my_eth_watch.rc /sdcard 
adb push my_eth_watch.sh /sdcard 
```

W root shell

```sh
mv /sdcard/my_eth_watch.sh /system/bin/
chown root:root /system/bin/my_eth_watch.sh
chmod 0500 /system/bin/my_eth_watch.sh

mv /sdcard/my_eth_watch.rc /etc/init/
chown 0.0 /etc/init/my_eth_watch.rc
chmod 0644 /etc/init/my_eth_watch.rc
chcon u:object_r:system_file:s0 /etc/init/my_eth_watch.rc

```

### Sprawdzenie

```
x86_64:/ $ dmesg | grep my_eth_watch
[    9.595354] init: Starting service 'my_eth_watch'...
```

```sh
ip route get 10.4.0.20
traceroute 10.4.0.20
```


### Użyteczne linki

- [Konfiguracja serwisu systemu .rc i skryptu .sh](https://android.stackexchange.com/questions/213353/how-to-run-an-executable-on-boot-and-keep-it-running)
- [Problem kilku tras routingu](https://kindlund.wordpress.com/2007/11/19/configuring-multiple-default-routes-in-linux/)

- - Plik `rt_tables` w androidzie znajduje się w `/data/misc/net/rt_tables `
- - Zamiast tworzenia nowej tabli można skorzystać z istniejącej np. `main` jak w skryptach w folderze