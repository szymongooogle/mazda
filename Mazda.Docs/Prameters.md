# Parametry

- Temperatura cpu
- Obciążenie ?

### Bt
- Widoczność bt
- Łączenie z telefonem 1, 2
- stan bt
- Ściszanie, pogłaśnienie mikrofonu
- Ściszanie, pogłaśnianie głośności rozmowy
- Następny utwór, poprzedni
- Odrzucanie, akceptowanie połączenia
- Utwór

### Sterowanie lcd
- Przyciski x6

### PT2258
- Głośność na kanale głównym
- Głośności na kanałach x3
- Wyciszenie, głósne kanały x3

### CAN
#### LS CAN
- Nawiew
- Blinda
- Temperatura (prawdopodobnie wody)
- Pozycja kluczyka
- Zasięg
- Spalanie / 100km
- Spalanie chwilowe
- Lewy kierunek
- Prawy kierunek
- Przyspieszenie - ls can
- Przędkość
- delta silnika ?

#### HS CAN
- Przebieg
- Kąt skrętu
- Przyspieszenie - hs can
- Torque ?
- Bieg w skrzyni AT
- Manual/Tiptronic
- GearShift
- Prędkość - hs can
- Przyspiszenie - hs can 2
- RPM

